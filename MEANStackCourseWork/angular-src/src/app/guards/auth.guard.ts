import { Injectable } from "@angular/core";
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { AuthService } from "../services/auth.service";

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private authService : AuthService,
        private router : Router){}

    canActivate() {
        if(this.authService.loggedInCheck()) {return true;}
        else {
            this.router.navigate(['/login']);
        }
    }
}