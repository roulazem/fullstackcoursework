import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'flash-messages-angular';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username : String;
  password : String;

  constructor( private authService : AuthService,
      private flashMessage : FlashMessagesService,
      private router : Router) { }

  ngOnInit() {
  }

  onLoginSubmit() {
    const user = {
      username :  this.username,
      password : this.password
    }

    this.authService.loginUser(user)
    .subscribe(data => {
      if(data.success) {
        this.authService.storeUserData(data.token, data.user)
        this.flashMessage.show("Successfully logged in", {cssClass: 'alert-success', timeout: 3000});
        this.router.navigate(['/dashboard'])
      } else {
        this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 3000});
        this.router.navigate(['/login']);
      }
    })

  }

}
