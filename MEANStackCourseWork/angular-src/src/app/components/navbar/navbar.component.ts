import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'flash-messages-angular';
import { timeout } from 'rxjs/operators';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private authService :AuthService,
    private router : Router,
    private flashMessages : FlashMessagesService) { }

  ngOnInit() {
  }

  onLogout() {
    this.authService.logout();
    this.flashMessages.show('Logged out!', { cssClass : 'alert-success', timeout : 3000 });
    this.router.navigate(["/login"]);
    return false;

  }

}
