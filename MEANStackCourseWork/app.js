const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoos = require("mongoose");
const passport = require("passport");
const config = require('./config/db');

//Connect to DB
mongoos.connect(config.database);

//Check on connection
mongoos.connection.on("connected", ()=> {
    console.log(`Connected to db:  ${config.database}`)
});

//Check on connection failure
mongoos.connection.on("error", (err)=> {
    console.log(`DB error:  ${err}`)
})

const app = express();
const port = 3000;

const users = require("./routes/users");
const { dirname } = require("path");

//Cors Middleware
app.use(cors());

//Set static folder
app.use(express.static(path.join(__dirname, 'public')));

// Body parser Middleware
app.use(bodyParser.json());

//passport Middleware
app.use(passport.initialize());
//app.use(passport.session());

require('./config/passport')(passport);

app.use("/users", users);

//Index route
app.get('/', (req,res)=> {
    res.send("Invalid endpoint")
})

app.get('*', (req,res)=> {
    res.sendFile(path.join(__dirname, "public/index.html"))
})

//Start server
app.listen(port, ()=> {
    console.log(`Server started at port ${port}`)
});