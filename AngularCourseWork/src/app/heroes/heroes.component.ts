
import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {


    //one hero instance
    // hero : Hero = {
    //   id: 1,
    //   name : 'Windstorm',
    // } ;
  
  // using list of heroes
  //selectedHero?: Hero; //The selectedHero property is not assigned any value
  heroes : Hero[] = [];


  constructor(private heroService: HeroService) { }

  ngOnInit(): void {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroService.getHeroes()
    .subscribe(heroes => this.heroes = heroes);
  }
  
  //Defining the method for selected hero
    // onSelect(hero: Hero) : void {
    //   this.messageService.add(`Hero component: selected hero with id ${hero.id}`)
    //   this.selectedHero = hero;
    // }

    add(name: string): void {
      name = name.trim();
      if (!name) { return; }
      this.heroService.addHero({ name } as Hero)
        .subscribe(hero => {
          this.heroes.push(hero);
        });
    }

    delete(hero: Hero): void {
      this.heroes = this.heroes.filter(h => h !== hero);
      this.heroService.deleteHero(hero.id).subscribe();
    }

}
