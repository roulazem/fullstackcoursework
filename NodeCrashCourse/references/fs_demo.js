const fs = require("fs");
const path = require("path");

//Create a dir
// fs.mkdir(path.join(__dirname, "/test"), {}, err => {
//     if(err) throw err;
//     console.log("Folder created...");
// });

//Create and write to file
// fs.writeFile(path.join(__dirname, "/test", "hello.txt"),
//  "Hello World.",
//  err => {
//     if(err) throw err;
//     console.log("File written to...");
//     //Update a file (Inside the callback to run togther with the write file in async)
//     fs.appendFile(path.join(__dirname, "/test", "hello.txt"), 
//     "\nI love Node.js",
//     err => {
//         if(err) throw err;
//         console.log("File updated...");
//     });
// });
//Read a file
fs.readFile(path.join(__dirname, "/test", "hello.txt"),
"utf-8",
(err, data) => {
   if(err) throw err;
   console.log(data);
});

//rename a file
fs.rename(path.join(__dirname, "/test", "hello.txt"),
path.join(__dirname, "/test", "helloWorld.txt"),
err => {
   if(err) throw err;
   console.log("File renamed...");
});