# FullStack

## Name
FullStack coursework. 

## Description

The repository includes all course related works each part in its own folder, as well as my own project. The project is an article library app.

## The Project

### Demo Video

https://www.kapwing.com/videos/62bcb77d20d21f0052316a2c

### Name

The ArticleApp project

### Description
Is an application for registered users to write their own articles, and share them with public. The published articles will be available for reading by the publics without a need to register. However, creating a favourite list for preferable or to read articles, is only allowed for registered users.

### Version 
V 1.0.0 

### Development tools

The project is developed using the latest versions of each 
Node v 16
Angular v 14

And used **Bootstrap 4** for html formatting

### Installation
Install node dependencies 

´´´
npm install 
´´´
and Angular dependencies

´´´
ng install
ng install  @auth0/angular-jwt
ng install flash-messages-angular
´´´

### Project Status

The project was developed as part of schoolwork. 

There is plenty of features and enhancements that can be done to it. For example, adding reactions and comment options on articles, updating own profile, enhancing some features' implementations 
Other suggestions are welcomed for further self-learning :) 

### Screenshots

Screenshot available on wiki

https://gitlab.com/roulazem/fullstackcoursework/-/wikis/Screen-Shots

## Authors and acknowledgment
Roula Almouayad Alazem, LAB university of applied science.
June 2022

