const express = require('express');
const router = express.Router();
const members = require('../../Members')
const uuid = require('uuid');
const { updateLocale } = require('moment');

//The get handler
// '/' is the index.js endpoint
// Every endpoint created, should have access to the req and res objects


// Get all members
router.get('/', (req,res)=> {
    res.json(members);
})

//Get single member api
router.get('/:id', (req,res)=> {

    const found = members.some(member => member.id === parseInt(req.params.id));

    if (found) {
        res.json(members.filter(member => member.id === parseInt(req.params.id)))
    } else {
        res.status(400).json({ msg : `Member with id ${req.params.id} not found`})
    }
    
});

//create a member 
router.post('/', (req,res)=> {
    const newMember = {
        id : uuid.v4(),
        name : req.body.name,
        email : req.body.email,
        status : 'active'
    }
    if(!newMember.name || !newMember.email) {
        return res.status(400).json({msg : 'Name and email are mandatory.'})
    }
    members.push(newMember);
    res.json(members);
})

//Update a member

router.put('/:id', (req,res)=> {

    const found = members.some(member => member.id === parseInt(req.params.id));

    if (found) {
        const UpdatedMember = req.body;
        members.forEach(member => {
            if(member.id === parseInt(req.params.id)) {
                member.name = UpdatedMember.name ? UpdatedMember.name : member.name;
                member.email = UpdatedMember.email ? UpdatedMember.email : member.email;
                member.status = UpdatedMember.status ? UpdatedMember.status : member.status;

                res.json({msg : `Member with id ${req.params.id} was updated`, member})
            }
        });
    } else {
        res.status(400).json({ msg : `Member with id ${req.params.id} not found`})
    }
});

// Delete a member
router.delete('/:id', (req,res)=> {
    const found = members.some(member => member.id === parseInt(req.params.id));
    if(found) {
        res.json({msg : `Member with id ${req.params.id} is deleted`,
            members: members.filter(member => member.id !== parseInt(req.params.id))})
    } else {
        res.status(400).json({ msg : `Member with id ${req.params.id} not found`})
    }
})

module.exports = router