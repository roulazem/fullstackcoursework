const express = require('express')
const path = require('path');
const logger = require('./Middleware/logger')
const router = require('./routes/api/members')

const app = express();

//Initiating the logger middleware
app.use(logger);

// Body parser middleware
app.use(express.json());
app.use(express.urlencoded({extended : false}));


//The get handler
// '/' is the index.js endpoint
// Every endpoint created, should have access to the req and res objects




// 1. attempt
// app.get('/', (req,res) => {
//     res.sendFile(path.join(__dirname, 'public', 'index.html'))
//     //res.send('<h1>Hello World</h1>');
// });

//2. attempt to set a static folder for different type of endpoints
app.use(express.static(path.join(__dirname, 'public')))

//Members API Routes
app.use('/api/members', router)


//check the port in the environment variable and use it,
// if not exist use port 5000

const PORT = process.env.PORT || 5000; 

app.listen(PORT, () => console.log(`Server started on port ${PORT}`)); // The second parameter is a callback