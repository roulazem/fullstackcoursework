const dbConfig = require('./db.config');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const userService = require('../services/user.service');


module.exports = function(passport) {
    //creating the options
    let opts = {};
    //using the autherisation header to extract the token
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
    //adding the key
    opts.secretOrKey= dbConfig.secret;

    passport.use(new JwtStrategy(opts, (jwt_payload, done)=> {
        userService.getUserById(jwt_payload.data._id, (err, user)=> {
            if(err){
                return done(err, false);
            }
            if(user) {
                return done(null, user);
            } else {
                return done (null, false);
            }
        });    
    }));
}