
const article = require("../models/article")


module.exports.createArticle = function(newArticle, callback) {
    newArticle.save(callback)
}

module.exports.getArticlesByUser = function(userId, callback) {
    const query = { user : userId}
    article.find(query, callback)
        .populate('user', "name");
    
}

module.exports.getAllPublishedArticles = function(published, callback) {
    const query = {published : published}
    article.find(query, callback)
        .populate('user', "name");
}

module.exports.getArticleById = function(id, callback) {
    const query = {_id : id}
    article.findById(query, callback)
        .populate('user', "name");
}

module.exports.getArticleByReaderId = function(id, callback) {
    const query = {readers : id}
    article.find(query, callback)
        .populate('user', "name")
}

module.exports.updateArticle = function(id, data, callback) {
    article.findByIdAndUpdate(id, data, callback)
}

module.exports.deleteArticleById = function(id, callback) {
    article.findByIdAndDelete(id, callback)
}

module.exports.addUserToArticle = function(id, user, callback) {
    
    const data = {$addToSet : {readers : user}}
    article.findByIdAndUpdate(id, data, callback)
}

module.exports.removeUserFromArticle = function(id, user, callback) {
    const data = {$pull : {readers : user}}
    article.findByIdAndUpdate(id, data, callback)
}

module.exports.removeUserFromMultipleArticles = function(user, callback) {
    const data = {$pull : {readers : user}}
    article.updateMany(data, callback)
}