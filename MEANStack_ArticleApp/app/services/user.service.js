const user  = require("../models/user");
const bcrypt = require("bcryptjs")

module.exports.createUser = function(newUser, callback) {
    bcrypt.genSalt(10, (err, salt)=> {
        bcrypt.hash(newUser.password, salt, (err, hash)=> {
            if(err) throw err;
            newUser.password = hash;
            newUser.save(callback)
        });
    });
}

module.exports.getUserByUsername = function(username, callback) {
    const query = {username : username}
    user.findOne(query, callback);
}

module.exports.getUserById = function(id, callback) {
    user.findById(id, callback)
}

module.exports.comparePassword = function(candidatePassword, hash, callback){
    bcrypt.compare(candidatePassword, hash, (err, isMatch)=> {
        if(err) throw err;
        callback(null, isMatch);
    });
}