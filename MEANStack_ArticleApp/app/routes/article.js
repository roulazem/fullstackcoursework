const express = require('express');
const router = express.Router();
const passport = require("passport");
const Article = require("../models/article");
const articleService = require("../services/article.service");

router.post("/",  passport.authenticate('jwt', {session : false}), (req,res,next) => {
    let newArticle = new Article({
        title : req.body.title,
        content : req.body.content,
        user: req.user._id,
    })
    articleService.createArticle(newArticle, (err, article) => {
        if(err) {
            res.json({success: false, msg: 'Failed to create the article'})
        } else {
           res.json({success: true, msg: 'Article created.'})
        }
    })
})

router.get("/", (req,res,next) => {
    const published = true;
    articleService.getAllPublishedArticles(published, (err, articles) => {
      if(err) {
          res.json({success: false, msg: 'Error occured while fetching articles'})
      } else {
          res.send({success: true, msg: articles})
      }
  }) 
})

router.get('/myArticles', passport.authenticate('jwt', {session : false}), (req,res,next)=> {
    const userId = req.user._id;
    articleService.getArticlesByUser(userId, (err, articles) => {
        if(err) {
            res.json({success: false, msg: `Error occured while fetching user's articles`})
        } else {
            res.send({success: true, msg: articles})
        }
    })
})

router.get('/favourites', passport.authenticate('jwt', {session : false}), (req,res,next)=> {
    const readerId = req.user._id;
    articleService.getArticleByReaderId(readerId, (err, articles) => {
        if(err) {
            res.json({success: false, msg: `Error occured while fetching user's favourites articles`})
        } else {
            res.send({success: true, msg: articles})
        }
    })
})

router.get('/:id', (req,res,next)=> {
    const id = req.params.id;
    articleService.getArticleById(id, (err, article) => {
        if(!article) {return  res.status(404).send({msg: `article not found`}) }
        if(err) {
            res.json({success: false, msg: `Error occured while fetching the required article`})
        } else {
            res.send({success: true, msg: article})
        }
    })
})



router.put('/:id', passport.authenticate('jwt', {session : false}), (req,res,next)=> {
    const id = req.params.id;
    const data = req.body;

    articleService.updateArticle(id, data, (err, article) => {      
        if(!article) {return  res.status(404).send({msg: `article not found`}) }
        
        if(err) {
            res.json({success: false, msg: `failed to update article`})
        } else {
            res.json({success: true, msg: `Article updated successfully`})
        }
    })
})

router.delete('/:id', passport.authenticate('jwt', {session : false}), (req,res,next) => {
    const id = req.params.id;
    
    articleService.deleteArticleById(id, (err, article) => {
        if(!article) {
            return res.status(404).send({msg : `Could not delete article. Article with ${id} was not found`})
        }
        if(err) {
            res.json({success: false, msg: `failed to delete article`})
        } else {
            res.json({success: true, msg: `Article deleted successfully`})
        }
    })
})

//The route to add/remove user to/from reader's list
router.put('/favourites/:id', passport.authenticate('jwt', {session : false}), (req,res,next)=> {
    const id = req.params.id;
    const user = req.user._id;

    articleService.addUserToArticle(id, user, (err, article) => {
        if(!article) {return  res.status(404).send({msg: `Article not found`}) }
        if(article.readers.includes(user)) {
            articleService.removeUserFromArticle(id, user, (err, article) => {
                if(err) {
                    res.json({success: false, msg: `failed to remove article to user`})
                } else {
                    
                    res.json({success: true, msg: `Article removed from your reading list`})
                }
            })
        } else {
            if(err) {
                res.json({success: false, msg: `failed to add article to user`})
            } else {
                
                res.json({success: true, msg: `Article added to your reading list`})
            }
        } 
    })

})

router.patch('/favourites', passport.authenticate('jwt', {session : false}), (req,res,next) => {
    
    const user = req.user._id;
    articleService.removeUserFromMultipleArticles(user, (err) => {
        if(err) {
            res.json({success: false, msg: `Failed to clear your list`})
        }else {
            res.json({success: true, msg: `Your list is cleared`})
        }
    })

})




module.exports =  router