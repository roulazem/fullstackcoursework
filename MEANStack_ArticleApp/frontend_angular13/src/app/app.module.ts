import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FlashMessagesModule } from 'flash-messages-angular';
import { FormsModule } from '@angular/forms';
import { AppRoutinModule } from './app-routin.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AddArticleComponent } from './components/add-article/add-article.component';
import { ArticleDetailsComponent } from './components/article-details/article-details.component';
import { HomeComponent } from './components/home/home.component';
import { MyArticlesComponent } from './components/my-articles/my-articles.component';
import { FavouritesComponent } from './components/favourites/favourites.component';
import { ValidationService } from './services/validation.service';
import { AboutComponent } from './components/about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    NavbarComponent,
    AddArticleComponent,
    ArticleDetailsComponent,
    HomeComponent,
    MyArticlesComponent,
    FavouritesComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutinModule,
    FlashMessagesModule.forRoot(),
    FormsModule,
    HttpClientModule,
  ],
  providers: [ValidationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
