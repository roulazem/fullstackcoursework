import { User } from "./user.model";


export class Article {
    _id? : any;
    title?: string;
    content?: string;
    published?: boolean;
    user?: {
        _id: string;
        name: string,
    };
    readers?: string[];
    createdAt?: string;
    updatedAt? : Date;
}
