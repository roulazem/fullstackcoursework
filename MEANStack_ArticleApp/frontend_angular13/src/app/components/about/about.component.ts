import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor(private router : Router) { }

  ngOnInit(): void {
  }

  getStarted() {
    this.router.navigate(["/home"])
  }

  demo() {
    window.open("https://www.kapwing.com/videos/62bcb77d20d21f0052316a2c", "_blank")
  }

}
