import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'flash-messages-angular';
import { Article } from 'src/app/models/article.model';
import { ArticleService } from 'src/app/services/article.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DataExchangeService } from 'src/app/services/data-exchange.service';

@Component({
  selector: 'app-favourites',
  templateUrl: './favourites.component.html',
  styleUrls: ['./favourites.component.css']
})
export class FavouritesComponent implements OnInit {

  articles : Article[] = []
  pendingArticle : Article = {}
  userId = ""
  
  currentArticle: Article = {};
  currentIndex = -1;

  constructor(private articleService: ArticleService,
    private authService : AuthenticationService,
    private flashMessage: FlashMessagesService,
    private router: Router,
    private dataExchange : DataExchangeService) { }

  ngOnInit(): void {
    const user = this.authService.loadUserData()
     if(user) {
      this.userId = user.id
     }
    this.getFavouriteList()
  }

  getFavouriteList() {
    this.articleService.getFavouriteArticles()
    .subscribe((res:any) => {
      if(res.success) {
        this.articles = res.msg;
        this.dataExchange.getCount(this.articles.length);
      } else {
        this.flashMessage.show(res.msg, {cssClass: 'alert-danger', timeout: 3000});
      }
    }); 
  }

  viewArticleDetails(article:Article, index: number) {
    this.currentArticle = article;
    this.currentIndex = index;
    this.router.navigate([`articles/${article._id}`]);    
  }

  removeArticle(article: Article) {
   
    this.articleService.addArticleFavourite(article._id, article)
    .subscribe((res : any) => {
      if(res.success) {
        this.articles = this.articles.filter(e => e !== article)
        this.dataExchange.getCount(this.articles.length);
        this.flashMessage.show(res.msg, {cssClass: 'alert-success', timeout: 3000});
        
      } else {
        this.flashMessage.show(res.msg, {cssClass: 'alert-danger', timeout: 3000});
      }
    })
    
  }

  clearFavouriteList() {
    this.articleService.clearFavourites(this.articles)
      .subscribe((res : any) => {
        if(res.success) {
          this.articles = []
          this.dataExchange.getCount(this.articles.length);          
        } else {
          this.flashMessage.show(res.msg, {cssClass: 'alert-danger', timeout: 3000});
        }
      })
     
    }     
}

//This function was done to add an article upon loggin. However, the function requires more work
// to check if the user is the articles writer (not fixed), and check if article already exist in favourites ( this part is fixed)
// Also it affects the badge in teh favourites nav button (not fixed)


// addPendingArticle() {
  //   const pending = localStorage.getItem('pendingFav')
  //   if(pending) {
      
  //     this.pendingArticle = JSON.parse(pending)
  //     localStorage.removeItem('pendingFav')
  //     this.articleService.getFavouriteArticles()
  //       .subscribe((res:any) => {
  //         if(res.success) {
  //           this.articles = res.msg;
  //           const found = this.articles.find(e => this.pendingArticle._id === e._id)
  //             if(!found) {
  //               this.articleService.addArticleFavourite(this.pendingArticle._id, this.pendingArticle)
  //                 .subscribe((res : any) => {
  //                   if(res.success) {
  //                     this.flashMessage.show('A new article is add to your favourite list', {cssClass : "alert-success", timeout : 2000});
  //                     this.dataExchange.getCount(this.articles.length + 1);
  //                   } else {
  //                     this.flashMessage.show('failed to add the article to your list', {cssClass : "alert-success", timeout : 2000})
  //                   }
  //                 }) 
  //             }  else {
  //               this.flashMessage.show('Article already exist in your list', {cssClass : "alert-warning", timeout : 2000})
  //             }
        
  //         } else {
  //           this.flashMessage.show(res.msg, {cssClass: 'alert-danger', timeout: 3000});
          
  //         }
  //     }); 
  //     }
  // }
