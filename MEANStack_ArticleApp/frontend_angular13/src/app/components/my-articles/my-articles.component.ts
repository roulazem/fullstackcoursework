import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'flash-messages-angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Article } from 'src/app/models/article.model';
import { ArticleService } from 'src/app/services/article.service';


@Component({
  selector: 'app-my-articles',
  templateUrl: './my-articles.component.html',
  styleUrls: ['./my-articles.component.css']
})
export class MyArticlesComponent implements OnInit {

  articles : Article[] = [];
  currentArticle: Article = {};
  currentIndex = 0;
  

  constructor(private articleService : ArticleService,
    private flashMessage: FlashMessagesService,
    private authService : AuthenticationService) { }

  ngOnInit(): void {
    this.getMyArticles();
  }

  getMyArticles(){
    this.articleService.getAllUsersArticles()
      .subscribe((res : any) => {
        if(res.success) {
          this.articles = res.msg;
          this.currentArticle = this.articles[this.currentIndex]
        } else {
          this.flashMessage.show(res.msg, {cssClass: 'alert-danger', timeout: 3000});
          
        }
      });
  }

  viewArticleDetails(article:Article, index: number) {
    this.currentArticle = article;
    this.currentIndex = index;
  }


  deletedArticleEvent(event: any){
    this.currentArticle = event;
    this.articles = this.articles.filter(e => e !== this.currentArticle)
    if(this.articles.length > 1) {
      this.currentIndex = this.currentIndex -1 
      this.currentArticle = this.articles[this.currentIndex]
    } else if (this.articles.length == 1){
      this.currentIndex = 0
      this.currentArticle = this.articles[this.currentIndex]
    } else {
      this.currentArticle = {}
    }
  }

  getUserName() {
    const user = this.authService.loadUserData()
    return user.name
  }

}
