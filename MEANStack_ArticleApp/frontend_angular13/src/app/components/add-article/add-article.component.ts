import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'flash-messages-angular';
import { Article } from 'src/app/models/article.model';
import { ArticleService } from 'src/app/services/article.service';
import { ValidationService } from 'src/app/services/validation.service';

@Component({
  selector: 'app-add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.css']
})
export class AddArticleComponent implements OnInit {

  article : Article = {
    title : "",
    content : ""
  }

  constructor(private articleService : ArticleService,
    private validationService: ValidationService,
    private flashMessage: FlashMessagesService,
    private router : Router) { }

  ngOnInit(): void {
  }

  createNewArticle() {

    const article = {
      title: this.article.title?.trim(),
      content: this.article.content
    }

    const valid = this.validationService.validateArticle(article);
      this.articleService.addNewArticle(article)
      .subscribe((res : any) => {
        if(res.success) {
          this.flashMessage.show(res.msg, {cssClass: 'alert-success', timeout: 3000});
          this.router.navigate(["/myArticles"])
        } else {
          if(!valid) {
            this.flashMessage.show("Please provide a title", {cssClass: 'alert-danger', timeout: 3000});
          } else {
            this.flashMessage.show(res.msg, {cssClass: 'alert-danger', timeout: 3000});
          }
          
        }
      })
  }

}
