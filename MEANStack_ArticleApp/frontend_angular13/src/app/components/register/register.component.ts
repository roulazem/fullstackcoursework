import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'flash-messages-angular';
import { User } from 'src/app/models/user.model';
import { ValidationService } from 'src/app/services/validation.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user : User = {
    name: undefined,
    email: undefined,
    username: undefined,
    password: undefined
  }
 

  constructor(private validationService: ValidationService,
    private userService : UserService,
    private flashMessage: FlashMessagesService,
    private router : Router) { }

  ngOnInit(): void {
  }


  onRegisterSubmit() {
    const user = {
      name: this.user.name?.trim(),
      email: this.user.email?.trim(),
      username: this.user.username?.trim(),
      password: this.user.password?.trim()
    };

    const validRegister = this.validationService.validateRegisteration(user)

    if(!validRegister){
      this.flashMessage.show("All fields are required", {cssClass: 'alert-danger', timeout: 3000});
      return false
    }

    const validEmail = this.validationService.validateEmail(user.email!)
    if(!validEmail) {
      this.flashMessage.show("Please provide a valid email address.", {cssClass: 'alert-danger', timeout: 3000});
      return false
    }  

    this.userService.createUser(user)
      .subscribe((res : any) => {  
        if(!res.success) {
          this.flashMessage.show(res.msg , {cssClass: 'alert-danger', timeout: 3000});
          this.router.navigate(['/register'])
        } else {
          this.flashMessage.show(res.msg, {cssClass: 'alert-success', timeout: 3000});
          this.router.navigate(['/login'])
        }
      });
      return true
    } 
  }
