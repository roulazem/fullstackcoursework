import { UpperCasePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'flash-messages-angular';
import { User } from 'src/app/models/user.model';
import { ArticleService } from 'src/app/services/article.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DataExchangeService } from 'src/app/services/data-exchange.service';
import { UserService } from 'src/app/services/user.service';
import { FavouritesComponent } from '../favourites/favourites.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user : User = {
    username: "",
    password: ""
  }

  constructor(private userService : UserService,
    private authService : AuthenticationService,
    private flashMessage: FlashMessagesService,
    private router: Router,
    private dataEx : DataExchangeService,
    private articleService: ArticleService) { }

  ngOnInit(): void {

    
  }

  onLoginSubmit() {
    const user = {
      username: this.user.username,
      password: this.user.password,
    }

    this.userService.loginUser(user)
      .subscribe((res :any) => {
        if(res.success) {
          const userName = res.user.name.toUpperCase()
          this.authService.storeUserData(res.token, res.user);
          this.flashMessage.show(`Welcome ${userName}`, {cssClass: 'alert-success', timeout: 3000} );
          this.router.navigate(['/home'])
        } else {
          this.flashMessage.show(res.msg, {cssClass: 'alert-danger', timeout: 3000});
          this.router.navigate(['/login'])
        }
      })
      
  }

}
