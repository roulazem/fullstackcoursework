import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'flash-messages-angular';

import { Article } from 'src/app/models/article.model';
import { ArticleService } from 'src/app/services/article.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DataExchangeService } from 'src/app/services/data-exchange.service';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  articles : Article[] = []
  loggedInView = false;

  currentArticle: Article = {};
  currentIndex = 0;
  indexList : number[] = []
  indexExist?: boolean;
  userId = "";

  constructor(private articleService: ArticleService,
    private flashMessage: FlashMessagesService,
    private router : Router,
    private authService: AuthenticationService,
    private dataExchange : DataExchangeService) { }

  ngOnInit(): void {
    
    
    this.AllPublishedArticles();
  }

  AllPublishedArticles() {
     const user = this.authService.loadUserData()
     if(user) {
      this.loggedInView = true;
      this.userId = user.id
     }
    // let FavComp = new FavouritesComponent(this.articleService, this.authService, this.flashMessage,this.router, this.dataExchange)
    // FavComp.addPendingArticle()
    this.articleService.getAllPublishedArticles()
      .subscribe((res: any) => {
        if(res.success) {
          this.articles = res.msg;
          this.articles.forEach(a => {
            if(a.readers?.includes(this.userId)) {
              this.indexList.push((this.articles.indexOf(a)))
              this.dataExchange.getCount(this.indexList.length);
            } 
           });
        } else {
          this.flashMessage.show(res.msg, {cssClass: 'alert-danger', timeout: 3000});
        }
        }
      )
  }


  viewArticleDetails(article:Article, index: number) {
    this.currentArticle = article;
    this.currentIndex = index;
    this.router.navigate([`articles/${article._id}`]);    
  }



  toggleFavourite(article : Article, index: number) {
    if(!this.indexList.includes(index)) {
      this.indexList.push(index);
      this.dataExchange.getCount(this.indexList.length);
    } else {
      const toRemove = this.indexList.indexOf(index)
      this.indexList.splice(toRemove, 1)
      this.dataExchange.getCount(this.indexList.length);
    }  
    article.readers?.push(this.userId);
    this.articleService.addArticleFavourite(article._id, article)
      .subscribe((res : any) => {
        if(res.success) {
          if(res.msg.includes("removed")) {
            this.flashMessage.show(res.msg, {cssClass: 'alert-warning', timeout: 2000});
          } else {
            this.flashMessage.show(res.msg, {cssClass: 'alert-success', timeout: 2000});
          }
          
        } else {
          this.flashMessage.show(res.msg, {cssClass: 'alert-danger', timeout: 3000});
        }
      })
  } 

  //The function was originally done to add to locall storage and then used in the addPendinToFav() function
  // which is declared in the favourites component

  addToPendingFavourite(article : Article) {
  //   const pending = JSON.stringify(article)
  //   localStorage.setItem('pendingFav' ,  pending)
     this.router.navigate(['login'])
   }
}

