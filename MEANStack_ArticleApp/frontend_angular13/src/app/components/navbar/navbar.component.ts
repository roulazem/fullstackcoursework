import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'flash-messages-angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DataExchangeService } from 'src/app/services/data-exchange.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  favCount = 0;


  constructor(private authService : AuthenticationService,
    private flashMessage: FlashMessagesService,
    private router : Router,
    private dataEx : DataExchangeService) { }


  ngOnInit(): void {
  }

  onLogout() {
    this.authService.logout();
    this.flashMessage.show('Logged out!', { cssClass : 'alert-success', timeout : 3000 });
    this.router.navigate(["/login"]);
    return false;
  }

  tokenExpired() {
    const loggedIn = this.authService.tokenExpiredCheck();
    return loggedIn;
  }

  goToAddArticle() {
    if(this.tokenExpired()) {
      this.flashMessage.show("You need to login to create an article!", { cssClass : 'alert-warning', timeout : 3000 })
      this.router.navigate(["/login"])
    } else {
      this.router.navigate(["/addArticle"])
    }
  }

  getUserName() {
    const user = this.authService.loadUserData()
    return user.name
  }

  getCount () {
    this.dataEx.favCount$
    .subscribe((count) => this.favCount = count)
    return this.favCount
  }
  
  
}
