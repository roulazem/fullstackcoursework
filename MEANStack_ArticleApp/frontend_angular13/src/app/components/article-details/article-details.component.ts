import { Parser } from '@angular/compiler';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FlashMessagesService } from 'flash-messages-angular';
import { Article } from 'src/app/models/article.model';
import { ArticleService } from 'src/app/services/article.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-article-details',
  templateUrl: './article-details.component.html',
  styleUrls: ['./article-details.component.css']
})
export class ArticleDetailsComponent implements OnInit {

  @Input()
  article : Article = {
    _id: "",
    title : "",
    content : "",
    published: false,
  }

  @Input() userMode :boolean = false;
  readerMode : boolean = true;
  writeMode = false;

  @Output() articleEvent = new EventEmitter<Article>()

  constructor(private articleService : ArticleService,
    private activatedRoute : ActivatedRoute,
    private flashMessage: FlashMessagesService) { }

  ngOnInit(): void {
    if(!this.userMode) {
      this.getArticleDetails();
    }
  }

  getArticleDetails() {

    const id = this.activatedRoute.snapshot.params['id'];
    //const id = this.article._id
    this.articleService.getArticleDetails(id)
      .subscribe((res : any) => {
        this.article = res.msg;
      })
  }

 openArticle(article: Article ) {
      this.writeMode = true
      this.userMode = false
      this.readerMode = false
  }

  publishArticle(published : boolean) {
    this.article.published = published;
    this.articleService.updateArticle(this.article._id, this.article)
      .subscribe((res : any) => {
        if(res.success) {
          const status = this.article.published ? "published" : "unpublished";
          this.flashMessage.show(`Article ${status} successfully`, {cssClass: 'alert-success', timeout: 3000});
        } else {
          this.flashMessage.show(res.msg, {cssClass: 'alert-danger', timeout: 3000});
        }
      })
  } 

  updateArticle(article: Article) {
    this.articleService.updateArticle(article._id, article)
      .subscribe((res: any) => {
        if(res.success) {
          this.flashMessage.show(res.msg, {cssClass: 'alert-success', timeout: 3000});
          //this.router.navigate(['myArticles']) //This change for details page
          this.userMode = true;
      
        } else {
          this.flashMessage.show(res.msg, {cssClass: 'alert-danger', timeout: 3000});
        }
      })
  }

  deleteArticle(article:Article) {
    this.articleEvent.emit(article)
    this.articleService.deleteArticle(article._id)
      .subscribe((res : any) => {
        if(res.success) {
          this.flashMessage.show(res.msg, {cssClass: 'alert-success', timeout: 3000});
          //this.router.navigate(['/myArticles'])
        } else {
          this.flashMessage.show(res.msg, {cssClass: 'alert-danger', timeout: 3000});
        }
        });
  }

  cancelButton() {
    window.location.reload()
  }
}
