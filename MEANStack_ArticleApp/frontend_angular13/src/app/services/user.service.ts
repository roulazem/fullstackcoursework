import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';
import { WebRequestService } from './web-request.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private webReqService : WebRequestService) { }

  createUser(user : User): Observable<User> {
    const req = this.webReqService.post('user/register', user);
    return req;
  }

  loginUser(user: User): Observable<User> {
    const req = this.webReqService.post('user/login', user);
    return req;
  }

}
