import { Injectable } from '@angular/core';
import { Article } from '../models/article.model';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor() { }

  validateRegisteration(user: User){
    if((user.name == undefined || user.name === "")||
      (user.email == undefined || user.email === "") ||
      (user.username == undefined || user.username === "") ||
      (user.password == undefined || user.password === "")) {
        return false;
      } else {
        return true;
      }
  }

  validateEmail(email: string) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  validateArticle(article: Article) {   
    if(article.title === ""){
      return false;
    } else {
      return true;
    }
  }

}

