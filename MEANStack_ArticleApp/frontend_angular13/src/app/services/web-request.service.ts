import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';

const baseUrl = 'http://localhost:3000';
let token : any;

@Injectable({
  providedIn: 'root'
})
export class WebRequestService {

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient,
    private authService : AuthenticationService) { }

  post(uri: string, data: any) {
    return this.http.post(`${baseUrl}/${uri}`, data , this.httpOptions)
  }

  get(uri: string): Observable<any> {
    return this.http.get(`${baseUrl}/${uri}`, this.httpOptions);
  }

  protectedPost(uri: string, data:any) {
    token = this.authService.loadToken();
    this.httpOptions = {
      headers : new HttpHeaders({
        'Authorization' : token,
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(`${baseUrl}/${uri}`, data , this.httpOptions)
  }

  protectedGet(uri: string): Observable<any> {
    token = this.authService.loadToken();
    this.httpOptions = {
      headers : new HttpHeaders({
        'Authorization' : token,
        'Content-Type': 'application/json'
      })
    };
    return this.http.get(`${baseUrl}/${uri}`, this.httpOptions);
  }

  put(uri: string, data:any): Observable<any> {
    token = this.authService.loadToken();
    this.httpOptions = {
      headers : new HttpHeaders({
        'Authorization' : token,
        'Content-Type': 'application/json'
      })
    };
    return this.http.put(`${baseUrl}/${uri}`, data, this.httpOptions);
  }

  patch(uri: string, data:any): Observable<any> {
    token = this.authService.loadToken();
    this.httpOptions = {
      headers : new HttpHeaders({
        'Authorization' : token,
        'Content-Type': 'application/json'
      })
    };
    return this.http.patch(`${baseUrl}/${uri}`, data,this.httpOptions);
  }

  delete(uri: string): Observable<any> {
    token = this.authService.loadToken();
    this.httpOptions = {
      headers : new HttpHeaders({
        'Authorization' : token,
        'Content-Type': 'application/json'
      })
    };
    return this.http.delete(`${baseUrl}/${uri}`, this.httpOptions);
    
  }

  
}
