import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { Article } from '../models/article.model';

@Injectable({
  providedIn: 'root'
})
export class DataExchangeService {

  constructor() { }

  private _favCountSource = new Subject<number>;

  favCount$ = this._favCountSource.asObservable();

  getCount(count : number) {
    this._favCountSource.next(count)
  }

}
