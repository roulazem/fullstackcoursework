import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Article } from '../models/article.model';
import { WebRequestService } from './web-request.service';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

   mainRoute = "articles";

  constructor(private webReqService : WebRequestService) { }

  getAllPublishedArticles() : Observable<Article[]> {
    const req = this.webReqService.get(this.mainRoute)
    return req;
  }

  getAllUsersArticles() : Observable<Article[]> {
    const req = this.webReqService.protectedGet(`${this.mainRoute}/myArticles`)
    return req;
  }

  getFavouriteArticles() : Observable<Article[]> {
    const req = this.webReqService.protectedGet(`${this.mainRoute}/favourites`)
    return req;
  }

  getArticleDetails(id: string) : Observable<Article>{
    const req = this.webReqService.get(`${this.mainRoute}/${id}`)
    return req;
  }

  addNewArticle(article : Article): Observable<Article> {
    
    const req = this.webReqService.protectedPost(this.mainRoute, article)
    return req;
  }

  deleteArticle(id: string): Observable<Article> {
    const req = this.webReqService.delete(`${this.mainRoute}/${id}`);
    return req;
  }

  updateArticle(id: string, article: Article): Observable<Article> {
    const req = this.webReqService.put(`${this.mainRoute}/${id}`, article);
    return req;
  }

  addArticleFavourite(id: string, article: Article): Observable<Article> {
    const req = this.webReqService.put(`${this.mainRoute}/favourites/${id}`, article);
    return req;
  }

  clearFavourites(articles: Article[]) {
    const req = this.webReqService.patch(`${this.mainRoute}/favourites`, articles);
    return req;
  }

}
