import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { JwtHelperService } from '@auth0/angular-jwt';




@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  authToken: any;
  user: User = {};
  jwtHelper = new JwtHelperService();

 
  constructor() { }

  
  storeUserData(token: string , user: User) {
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user))
  }

  loadToken() {
    const token = localStorage.getItem('id_token');
    this.authToken = token
    return token;
  }

  loadUserData() {
    const user = localStorage.getItem('user')
    return JSON.parse(user!)
  }

  tokenExpiredCheck() {
    const token = this.loadToken();
    return this.jwtHelper.isTokenExpired(token!);
    
  }

  logout() {
    this.authToken = null;
    this.user = {};
    localStorage.clear()
  }
}
