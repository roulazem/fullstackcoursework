const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const dbConfig = require("./app/config/db.config")
const path = require("path");
const bodyParser = require("body-parser");
const passport = require("passport")

const db = dbConfig.url;

//connect to DB
mongoose.connect(db);

//check connection
mongoose.connection.on("connected", ()=> {
    console.log(`Connected to DB: ${db}`)
})


const app = express();
const port = process.env.PORT || 3000;

const user = require('./app/routes/user');
const articles = require('./app/routes/article');

app.use(cors());

app.use(express.static(path.join(__dirname, 'public')))

// Body parser Middleware
app.use(bodyParser.json());

//passport Middleware
app.use(passport.initialize());

require('./app/config/passport.config')(passport);

app.use("/user", user);
app.use("/articles", articles);

//index route
app.get("/", (req,res) => {
    res.send("Welcome")
})


app.get("*", (req,res)=> {
    res.send(path.join(__dirname, 'public/index.html'))
})

app.listen(port, () => {
    console.log(`Server started at port ${port}`)
})